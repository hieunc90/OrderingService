package com.esoft.orderingservice.repository;

import com.esoft.orderingservice.dto.SummaryDataResponse;
import com.esoft.orderingservice.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    @Modifying
    @Query("DELETE FROM Order o WHERE o.id = :orderId AND o.createdBy = :userId")
    public void deleteOrder (Long orderId, Long userId);

    public Order findByIdAndCreatedBy (Long orderId, Long userId);

    @Query("SELECT o FROM Order o WHERE o.createdBy = :userId")
    public List<Order> findAllByCreatedBy (Long userId);
    @Query("SELECT count(o) AS numberOfOrder FROM Order o WHERE o.createdBy = :userId")
    public Long getNumberOfOrder (Long userId);
    @Query("SELECT sum (o.quantity) AS revenue FROM Order o WHERE o.createdBy = :userId")
    public Long getRevenue (Long userId);

    @Query("SELECT new com.esoft.orderingservice.dto.SummaryDataResponse(count(o),sum (o.quantity)) FROM Order o " +
            " WHERE o.createdBy = :userId" +
            " AND o.createdAt BETWEEN :from AND :to")
    public SummaryDataResponse getRevenueAndNumberOfOrder (Long userId, Date from, Date to);
}
