package com.esoft.orderingservice.enums;

import lombok.Getter;

@Getter
public enum OrderCategory {
    LUXURY (1,"LUXURY"),
    SUPER_LUXURY(2,"SUPER_LUXURY"),
    SUPREME_LUXURY(3,"SUPREME_LUXURY");

    private int type;
    private String name;

    private OrderCategory (int type, String name){
        this.type = type;
        this.name = name;
    }
}
