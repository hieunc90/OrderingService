package com.esoft.orderingservice.enums;

import lombok.Getter;

public enum UserRole {
    ADMIN (1),
    CUSTOMER (2);

    @Getter
    private int role;

    UserRole(int role) {
        this.role = role;
    }
}
