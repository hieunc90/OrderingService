package com.esoft.orderingservice.enums;

import lombok.Getter;

@Getter
public enum OrderServiceType {
    PHOTO_EDITING (1,"PHOTO_EDITING"),
    VIDEO_EDITING (2,"VIDEO_EDITING");

    private int type;
    private String name;
    private OrderServiceType(int type, String name){
        this.type = type;
        this.name = name;
    }
}
