package com.esoft.orderingservice.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class ApplicationException extends Exception{
    private Integer errorCode;
    private String message;
    public ApplicationException(Integer errorCode){
        this.errorCode = errorCode;
    }
}
