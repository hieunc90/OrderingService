package com.esoft.orderingservice.constant;

public class ErrorCode {
    public static final Integer PARAM_INVALID = 1;
    public static final Integer NOT_AUTHORIZED = 2;
    public static final Integer OK = 200;
    public static final Integer INTERNAL_SERVER = 500;
}
