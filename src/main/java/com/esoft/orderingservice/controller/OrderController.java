package com.esoft.orderingservice.controller;

import com.esoft.orderingservice.constant.ApplicationException;
import com.esoft.orderingservice.constant.ErrorCode;
import com.esoft.orderingservice.dto.BaseResponseData;
import com.esoft.orderingservice.dto.OrderRequest;
import com.esoft.orderingservice.dto.OrderResponse;
import com.esoft.orderingservice.interceptor.UserSession;
import com.esoft.orderingservice.service.OrderService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Log4j2
@RequestMapping("/order")
@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;
    @Autowired
    UserSession userSession;

    @GetMapping("/{id}")
    public BaseResponseData<OrderResponse> getOrder (@PathVariable Long id) {
        try {
            OrderRequest orderRequest = new OrderRequest();
            orderRequest.setId(id);
            orderRequest.setUserId(userSession.getId());
            return new BaseResponseData<OrderResponse>(ErrorCode.OK,null, orderService.getOrder(orderRequest));
        }catch (ApplicationException a){
            log.error(a);
            return new BaseResponseData<OrderResponse>(a.getErrorCode(),a.getMessage(), null);
        }catch (Exception e) {
            log.error(e);
            return new BaseResponseData<OrderResponse>(ErrorCode.INTERNAL_SERVER,null, null);
        }

    }
    @GetMapping("/all-orders")
    public BaseResponseData<List<OrderResponse>> getOrder () {
        try {
            OrderRequest orderRequest = new OrderRequest();
            orderRequest.setUserId(userSession.getId());
            return new BaseResponseData<List<OrderResponse>>(ErrorCode.OK,null, orderService.getListOrder(orderRequest));
        }catch (ApplicationException a){
            log.error(a);
            return new BaseResponseData<>(a.getErrorCode(),a.getMessage(), null);
        }catch (Exception e) {
            log.error(e);
            return new BaseResponseData<>(ErrorCode.INTERNAL_SERVER,null, null);
        }
    }

    @PostMapping
    public BaseResponseData<OrderResponse> createOrder (@RequestBody OrderRequest orderRequest) {
        try {
            orderRequest.setUserId(userSession.getId());
            return new BaseResponseData<OrderResponse>(ErrorCode.OK, null, orderService.createOrder(orderRequest));
        } catch (ApplicationException a) {
            log.error(a);
            return new BaseResponseData<OrderResponse>(a.getErrorCode(), a.getMessage(), null);
        } catch (Exception e) {
            log.error(e);
            return new BaseResponseData<OrderResponse>(ErrorCode.INTERNAL_SERVER, null, null);
        }
    }

    @PutMapping
    public BaseResponseData<OrderResponse> updateOrder (@RequestBody OrderRequest orderRequest) {
        try {
            orderRequest.setUserId(userSession.getId());
            return new BaseResponseData<OrderResponse>(ErrorCode.OK, null, orderService.updateOrder(orderRequest));
        } catch (ApplicationException a) {
            log.error(a);
            return new BaseResponseData<OrderResponse>(a.getErrorCode(), a.getMessage(), null);
        } catch (Exception e) {
            log.error(e);
            return new BaseResponseData<OrderResponse>(ErrorCode.INTERNAL_SERVER, null, null);
        }
    }

    @DeleteMapping
    public BaseResponseData<OrderResponse> deleteOrder (@RequestBody OrderRequest orderRequest) {
        try {
            orderRequest.setUserId(userSession.getId());
            orderService.deleteOrder(orderRequest);
            return new BaseResponseData<OrderResponse>(ErrorCode.OK, null, null);
        } catch (ApplicationException a) {
            log.error(a);
            return new BaseResponseData<OrderResponse>(a.getErrorCode(), a.getMessage(), null);
        } catch (Exception e) {
            log.error(e);
            return new BaseResponseData<OrderResponse>(ErrorCode.INTERNAL_SERVER, null, null);
        }
    }
}
