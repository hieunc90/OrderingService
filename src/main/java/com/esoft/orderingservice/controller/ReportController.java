package com.esoft.orderingservice.controller;

import com.esoft.orderingservice.constant.ApplicationException;
import com.esoft.orderingservice.constant.ErrorCode;
import com.esoft.orderingservice.dto.BaseResponseData;
import com.esoft.orderingservice.dto.OrderRequest;
import com.esoft.orderingservice.dto.OrderResponse;
import com.esoft.orderingservice.dto.SummaryDataResponse;
import com.esoft.orderingservice.interceptor.UserSession;
import com.esoft.orderingservice.service.ReportService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;

@Log4j2
@RequestMapping("/reports")
@RestController
public class ReportController {

    @Autowired
    UserSession userSession;
    @Autowired
    ReportService reportService;

    @GetMapping("/number-of-order")
    public BaseResponseData<SummaryDataResponse> getNumberOfOrder () {
        try {
            return new BaseResponseData<>(ErrorCode.OK,null, reportService.getNumberOfOrder(userSession.getId()));
        }catch (ApplicationException a){
            log.error(a);
            return new BaseResponseData<>(a.getErrorCode(),a.getMessage(), null);
        }catch (Exception e) {
            log.error(e);
            return new BaseResponseData<>(ErrorCode.INTERNAL_SERVER,null, null);
        }

    }

    @GetMapping("/revenue")
    public BaseResponseData<SummaryDataResponse> getRevenue () {
        try {
            return new BaseResponseData<>(ErrorCode.OK,null, reportService.getRevenue(userSession.getId()));
        }catch (ApplicationException a){
            log.error(a);
            return new BaseResponseData<>(a.getErrorCode(),a.getMessage(), null);
        }catch (Exception e) {
            log.error(e);
            return new BaseResponseData<>(ErrorCode.INTERNAL_SERVER,null, null);
        }

    }

    @GetMapping("/number-of-order-and-revenue")
    public BaseResponseData<SummaryDataResponse> getRevenueAndNumberOfOrderInPeriod (@RequestParam String from, @RequestParam String to) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            return new BaseResponseData<>(ErrorCode.OK,null, reportService.getRevenueAndNumberOfOrderInPeriod(userSession.getId(),simpleDateFormat.parse(from),simpleDateFormat.parse(to)));
        }catch (ApplicationException a){
            log.error(a);
            return new BaseResponseData<>(a.getErrorCode(),a.getMessage(), null);
        }catch (Exception e) {
            log.error(e);
            return new BaseResponseData<>(ErrorCode.INTERNAL_SERVER,null, null);
        }

    }
}
