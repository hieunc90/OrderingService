package com.esoft.orderingservice.interceptor;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Getter @Setter
public class UserSession {
    private Long id;
}
