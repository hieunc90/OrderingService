package com.esoft.orderingservice.model;

import com.esoft.orderingservice.enums.OrderCategory;
import com.esoft.orderingservice.dto.OrderRequest;
import com.esoft.orderingservice.enums.OrderServiceType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    @NonNull
    private Integer category;
    @NonNull
    private Integer service;
    @NonNull
    @Column(name = "reference_string")
    private String reference;
    @NonNull
    private Integer quantity;
    private String description;
    private String notes;

    @Column(name = "created_at")
    private Date createdAt;
    @NonNull
    @Column(name = "created_by")
    private Long createdBy;


    public Order(OrderRequest orderRequest) {
        this.id = orderRequest.getId();
        this.category = orderRequest.getCategory();
        this.service = orderRequest.getService();
        this.reference = orderRequest.getReference();
        this.quantity = orderRequest.getQuantity();
        this.description = orderRequest.getDescription();
        this.createdBy = orderRequest.getUserId();
        this.notes = orderRequest.getNotes();
        this.createdAt = Calendar.getInstance().getTime();
    }

    public OrderCategory getCategoryType (Integer type) {
        for (OrderCategory categoryType : OrderCategory.values()) {
            if (categoryType.getType() == this.getCategory()) {
                return categoryType;
            }
        }
        return null;
    }

    public OrderServiceType getServiceType (Integer type) {
        for (OrderServiceType serviceType : OrderServiceType.values()) {
            if (serviceType.getType() == this.getService()) {
                return serviceType;
            }
        }
        return null;
    }
}
