package com.esoft.orderingservice.service;

import com.esoft.orderingservice.constant.ApplicationException;
import com.esoft.orderingservice.constant.ErrorCode;
import com.esoft.orderingservice.dto.SummaryDataResponse;
import com.esoft.orderingservice.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ReportServiceImpl implements ReportService{

    @Autowired
    private OrderRepository orderRepository;


    /**
     * Get summary number of orders placed by a user
     * @param userId
     * @return
     * @throws Exception
     */
    @Override
    public SummaryDataResponse getNumberOfOrder(Long userId) throws Exception {
        if (userId == null ) {
            throw new ApplicationException(ErrorCode.PARAM_INVALID);
        }
        Long numberOfOrder = orderRepository.getNumberOfOrder(userId);
        return new SummaryDataResponse(numberOfOrder,null);
    }

    /**
     * Get summary revenue from a user
     * @param userId
     * @return
     * @throws Exception
     */
    @Override
    public SummaryDataResponse getRevenue(Long userId) throws Exception {
        if (userId == null ) {
            throw new ApplicationException(ErrorCode.PARAM_INVALID);
        }
        Long revenue = orderRepository.getRevenue(userId);
        return new SummaryDataResponse(null,revenue);
    }

    /**
     * Get summary number of orders and revenue placed via this service within a specific of
     * time
     * @param userId
     * @param from
     * @param to
     * @return
     * @throws Exception
     */
    @Override
    public SummaryDataResponse getRevenueAndNumberOfOrderInPeriod(Long userId, Date from, Date to) throws Exception {
        if (userId == null || from == null || to == null) {
            throw new ApplicationException(ErrorCode.PARAM_INVALID);
        }
        return orderRepository.getRevenueAndNumberOfOrder(userId, from, to);
    }
}
