package com.esoft.orderingservice.service;

import com.esoft.orderingservice.dto.SummaryDataResponse;

import java.util.Date;

public interface ReportService {
    public SummaryDataResponse getNumberOfOrder (Long userId) throws Exception;
    public SummaryDataResponse getRevenue (Long userId) throws Exception;
    public SummaryDataResponse getRevenueAndNumberOfOrderInPeriod (Long userId, Date from, Date to) throws Exception;
}
