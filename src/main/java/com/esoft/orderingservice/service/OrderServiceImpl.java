package com.esoft.orderingservice.service;

import com.esoft.orderingservice.constant.ApplicationException;
import com.esoft.orderingservice.constant.ErrorCode;
import com.esoft.orderingservice.dto.OrderRequest;
import com.esoft.orderingservice.dto.OrderResponse;
import com.esoft.orderingservice.enums.UserRole;
import com.esoft.orderingservice.model.Order;
import com.esoft.orderingservice.model.User;
import com.esoft.orderingservice.repository.OrderRepository;
import com.esoft.orderingservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService{

    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private UserRepository userRepository;


    /**
     * create an order for a user
     * @param orderRequest
     * @return
     * @throws Exception
     */
    @Override
    public OrderResponse createOrder(OrderRequest orderRequest) throws Exception {
        if(!this.isCustomer(orderRequest.getUserId())) {
            throw new ApplicationException(ErrorCode.NOT_AUTHORIZED);
        }
        if (!orderRequest.valid()) {
            throw new ApplicationException(ErrorCode.PARAM_INVALID);
        }
        Order order = new Order(orderRequest);
        Order result = orderRepository.save(order);
        return new OrderResponse(result);
    }

    /**
     * update an order by owner
     * suppose that only owner can update order
     * @param orderRequest
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public OrderResponse updateOrder(OrderRequest orderRequest) throws Exception {
        if(!this.isCustomer(orderRequest.getUserId())) {
            throw new ApplicationException(ErrorCode.NOT_AUTHORIZED);
        }
        if (!orderRequest.valid()) {
            throw new ApplicationException(ErrorCode.PARAM_INVALID);
        }
        Order order = new Order(orderRequest);
        Order result = orderRepository.save(order);
        return new OrderResponse(result);
    }

    /**
     * delete an order by owner
     * suppose that only owner can delete it
     * @param orderRequest
     * @throws Exception
     */
    @Override
    @Transactional
    public void deleteOrder(OrderRequest orderRequest) throws Exception {
        if (orderRequest == null || orderRequest.getUserId() == null || orderRequest.getUserId() == null) {
            throw new ApplicationException(ErrorCode.PARAM_INVALID);
        }
        if(!this.isCustomer(orderRequest.getUserId())) {
            throw new ApplicationException(ErrorCode.NOT_AUTHORIZED);
        }
        orderRepository.deleteOrder(orderRequest.getId(), orderRequest.getUserId());
    }

    /**
     * get specified order by owner
     * @param orderRequest
     * @return
     * @throws Exception
     */
    @Override
    public OrderResponse getOrder(OrderRequest orderRequest) throws Exception {
        if (orderRequest.getId() == null || orderRequest.getUserId() == null) {
            throw new ApplicationException(ErrorCode.PARAM_INVALID);
        }
        Order rs = orderRepository.findByIdAndCreatedBy(orderRequest.getId(),orderRequest.getUserId());
        return rs == null ? null : new OrderResponse(rs);
    }

    /**
     * get all orders of owner
     * @param orderRequest
     * @return
     * @throws Exception
     */
    @Override
    public List<OrderResponse> getListOrder(OrderRequest orderRequest) throws Exception {
        if (orderRequest.getUserId() == null) {
            throw new ApplicationException(ErrorCode.PARAM_INVALID);
        }
        List<Order> orders = orderRepository.findAllByCreatedBy(orderRequest.getUserId());
        return orders.stream().map(order -> new OrderResponse(order)).collect(Collectors.toList());
    }

    /**
     * check an user with specified userId whether customer or not
     * @param userId
     * @return
     */
    public boolean isCustomer (Long userId) {
        User user = userRepository.findById(userId).get();
        if (user != null && user.getRole() == UserRole.CUSTOMER.getRole()) {
            return true;
        }
        return false;
    }
    public boolean checkUser (Long userId) {
        return true;
    }
}
