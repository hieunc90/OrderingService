package com.esoft.orderingservice.service;

import com.esoft.orderingservice.dto.OrderRequest;
import com.esoft.orderingservice.dto.OrderResponse;

import java.util.List;

public interface OrderService {
    /**
     * create new order by a user
     * @param orderRequest
     * @throws Exception
     */
    public OrderResponse createOrder (OrderRequest orderRequest) throws Exception;

    /**
     * update an order by owner
     * @param orderRequest
     * @throws Exception
     */
    public OrderResponse updateOrder (OrderRequest orderRequest) throws Exception;

    /**
     * delete an order by owner
     * @param orderRequest
     * @throws Exception
     */
    public void deleteOrder (OrderRequest orderRequest) throws Exception;

    /**
     * get an order by its owner and its id
     * @param orderRequest
     * @return
     * @throws Exception
     */
    public OrderResponse getOrder (OrderRequest orderRequest) throws Exception;

    /**
     * get all order of owner
     * @param orderRequest
     * @return
     * @throws Exception
     */
    public List<OrderResponse> getListOrder(OrderRequest orderRequest) throws Exception;
}
