package com.esoft.orderingservice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.NonNull;

import javax.persistence.Id;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderRequest {

    private static final Logger LOG = LoggerFactory.getLogger(OrderRequest.class);
    private Long id;
    @NonNull
    private Integer category;
    @NonNull
    private Integer service;
    @NonNull
    private String reference;
    private Integer quantity;
    private String description;
    private String notes;
    @NonNull
    private Long userId;

    public boolean valid () {
        try {
            if (category <= 0 || service <= 0 || reference == null || userId == null) {
                return false;
            }
            return true;
        }catch (Exception e) {
            LOG.error(e.getMessage());
            return false;
        }
    }
}
