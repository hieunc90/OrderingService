package com.esoft.orderingservice.dto;

import com.esoft.orderingservice.enums.OrderCategory;
import com.esoft.orderingservice.enums.OrderServiceType;
import com.esoft.orderingservice.model.Order;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.lang.NonNull;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderResponse {
    private Long id;
    @NonNull
    private OrderCategory category;
    @NonNull
    private OrderServiceType service;
    @NonNull
    private String reference;
    @NonNull
    private Integer quantity;
    private String description;
    private String notes;

    public OrderResponse (Order order) {
        this.id = order.getId();
        this.category = order.getCategoryType(order.getCategory());
        this.service = order.getServiceType(order.getService());
        this.reference = order.getReference();
        this.quantity = order.getQuantity();
        this.description = order.getDescription();
        this.notes = order.getNotes();
    }
}
