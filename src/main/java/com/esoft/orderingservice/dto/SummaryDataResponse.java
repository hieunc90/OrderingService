package com.esoft.orderingservice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class SummaryDataResponse {
    private Long numberOfOrder;
    private Long revenue;
}
