package com.esoft.orderingservice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class BaseResponseData <T>{
    private int errorCode;
    private String message;
    private T data;
}
