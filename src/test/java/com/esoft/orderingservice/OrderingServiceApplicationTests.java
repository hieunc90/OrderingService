package com.esoft.orderingservice;

import com.esoft.orderingservice.dto.OrderRequest;
import com.esoft.orderingservice.dto.SummaryDataResponse;
import com.esoft.orderingservice.service.OrderService;
import com.esoft.orderingservice.service.ReportService;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Calendar;
import java.util.Date;

@SpringBootTest
class OrderingServiceApplicationTests {

    @Autowired
    ReportService reportService;
    @Autowired
    OrderService orderService;
    @Test
    void contextLoads() {
        assertTrue(true);
    }

    @BeforeEach
    public void createOrder(){
        OrderRequest orderRequest = new OrderRequest();
        orderRequest.setId(1L);
        orderRequest.setUserId(4L);
        orderRequest.setReference("unit test 1");
        orderRequest.setService(1);
        orderRequest.setCategory(1);
        orderRequest.setDescription("unit test");
        orderRequest.setQuantity(3);
        OrderRequest anotherOrder = new OrderRequest();
        anotherOrder.setId(2L);
        anotherOrder.setUserId(4L);
        anotherOrder.setReference("unit test 2");
        anotherOrder.setService(2);
        anotherOrder.setCategory(2);
        anotherOrder.setDescription("unit test");
        anotherOrder.setQuantity(8);
        try {
            assertAll("Test create order",()->orderService.createOrder(orderRequest),()->orderService.createOrder(anotherOrder));
        }catch (Exception e){
        }
    }
    @Test
    public void summaryOrder(){
        try {
            assertAll(()->assertEquals(2, reportService.getNumberOfOrder(4L).getNumberOfOrder()));
        }catch (Exception e) {

        }
    }
    @Test
    public void summaryRevenue(){
        try {
            assertAll(() -> assertEquals(11, reportService.getRevenue(4L).getRevenue()));
        }catch (Exception e){

        }
    }
    @Test
    public void summaryRevenueAndNumberOfOrder (){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DATE,calendar.get(Calendar.DATE) -1);
        Date yesterday = calendar.getTime();
        calendar.set(Calendar.DATE,calendar.get(Calendar.DATE) + 2);
        Date tomorrow = calendar.getTime();
        try {
            SummaryDataResponse summaryDataResponse = reportService.getRevenueAndNumberOfOrderInPeriod(4L, yesterday, tomorrow);
            assertEquals(2,summaryDataResponse.getNumberOfOrder());
            assertEquals(11,summaryDataResponse.getRevenue());
            summaryDataResponse = reportService.getRevenueAndNumberOfOrderInPeriod(4L, yesterday, yesterday);
            assertEquals(0,summaryDataResponse.getNumberOfOrder() == null ? 0 : summaryDataResponse.getNumberOfOrder());
            assertEquals(0,summaryDataResponse.getRevenue() == null ? 0 : summaryDataResponse.getRevenue());
        }catch (Exception e){

        }
    }
}
