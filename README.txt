A.Run application:
    I. Presiquisite
        1.Install MySQL in database server
        2.Install Maven and JDK 8 in application server
    II. Step
        1.Create table user
        `create table users (
         id serial,
         user_role tinyint not null,
         primary key (id));`
        2.Create table orders
        `create table orders (
            id serial,
             reference_string varchar(100) not null unique,
             category tinyint not null,
             service tinyint not null,
             quantity int default 0,
             description varchar (200),
             notes varchar (100),
             created_at timestamp,
             created_by bigint not null,
             primary key (id)
         );`
        3.Add users into table users. Sure that have user with id = 4 (application will run with this user)
        4.Download source to application server via link Gitlab:
        5.Run command: mvn spring-boot:run -Dspring-boot.run.jvmArguments="-DDB.PASSWORD=${PASSWORD} -DB.SCHEMA=${SCHEMA}
          -DB.HOSTNAME=${HOSTNAME} -DB.USERNAME=${USERNAME}"
          from root folder of project
        Replace each phrase ${...} by appropriate value
        Example: mvn spring-boot:run -Dspring-boot.run.jvmArguments="-DDB.PASSWORD=esoft -DDB.SCHEMA=exam -DDB.HOSTNAME=jdbc:mysql://localhost:3306/exam -DDB.USERNAME=root"
    III.Unit test
        1.Create database test with same structure database server
        2.run mvn test -DDB.PASSWORD=${PASSWORD} -DB.SCHEMA=${SCHEMA}
                        -DB.HOSTNAME=${HOSTNAME} -DB.USERNAME=${USERNAME}
          from root folder of project like II.5 except value of HOSTNAME point to database test
        Example:mvn test -DDB.PASSWORD=esoft -DDB.SCHEMA=exam_test -DDB.HOSTNAME=jdbc:mysql://localhost:3306/exam_test
        -DDB.USERNAME=root
B.Appendix
    I.Endpoint
        1.Ordering
            GET:   /order/{id}
                   /order//all-orders
            POST:  /order
            PUT:   /order
            DELETE:/order
        2.Report
            /reports/number-of-order
            /reports/revenue
            /reports/number-of-order-and-revenue?from=&to=
